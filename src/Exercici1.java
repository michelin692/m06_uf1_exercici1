
import java.io.File;
import java.io.FileFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miguel
 */
public class Exercici1{
     
    
    public static void listarDirectorio(File archivo , FileFilter filtro ){
         
         File archivos[] = archivo.listFiles(filtro);
         for(int indice = 0; indice < archivos.length; indice++){
            if( archivos[indice].isDirectory()  ){
              listarDirectorio(archivos[indice] , filtro );
            }else{
                System.out.println(archivos[indice].getPath());
            }
        }       
    }
     
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args ) {
        
        try{
            //System.out.println(args[0]);
            //CONTROLES DE ERRORr
            //tiene más de dos parametros
            if(args.length > 2){
                System.out.println("Tiene más de dos parametros");
                System.exit(2);
            } ;

            //El primer paràmetre no correspon a una carpeta del sistema de fitxers
            File fichero = new File(args[0]);
            if(!fichero.isDirectory()){
                System.out.println("No tiene ninguna T o numero entero");
                System.exit(2);
            } ;
            
            
            //El segon paràmetre no és ni una ‘t’ ni una ‘T’ ni qualsevol nombre enter
             String argmuento2 = args[1].toLowerCase();
            if( !argmuento2.equalsIgnoreCase("t") ){
                System.out.println("No tiene ninguna T o numero entero");
                System.exit(2);
            } ;
            
            //preparo el filtro que después utilizaré en funcion recursiva.
            FileFilter filtro = (File archivo) -> {
                return (archivo.isDirectory() || archivo.isFile() && !archivo.isHidden() );
            };
                
            //llamo a metodo recursivo para pintar directorios.
            listarDirectorio(fichero, filtro); 
            
            
            
        }catch(Exception e){
            System.out.println("Error: "+e.getMessage());
        }    
         
        
    }
    
    
    
}


